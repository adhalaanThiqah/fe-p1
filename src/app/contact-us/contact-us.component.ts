import { Component } from '@angular/core';
import { Contact } from '../model/contact';
import { ContactService } from '../services/contact.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['../../assets/scss/contact-us.component.scss'],
})
export class ContactUsComponent {
  constructor(private cs: ContactService) {}
  submit(obj: Contact) {
    this.cs.submitContactForm(obj);
    console.log(obj);
  }
}
