export class Contact {
  id?: number;
  name: string;
  email: string;
  phone: string;
  findus: string;
  constructor() {
    this.name = '';
    this.email = '';
    this.phone = '';
    this.findus = '';
  }
}
