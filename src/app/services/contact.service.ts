import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contact } from '../model/contact';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  constructor(private http: HttpClient) {}

  submitContactForm(contact: Contact) {
     this.http.post<Contact>('localhost:4000/submit', JSON.stringify(contact));
  }
}
