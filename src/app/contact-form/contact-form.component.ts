import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Contact } from '../model/contact';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['../../assets/scss/contact-form.component.scss'],
})
export class ContactFormComponent implements OnInit {
  @Output() formOut: EventEmitter<Contact> = new EventEmitter<Contact>();
  registerForm!: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }
  initForm(): void {
    this.registerForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', []],
      phone: ['', [Validators.required]],
      findus: ['', ''],
    });
  }
  submitForm() {
    this.formOut.emit({
      name: this.name.getRawValue(),
      email: this.email.getRawValue(),
      phone: this.phone.getRawValue(),
      findus: this.findus.getRawValue(),
    });
  }
  get name(): AbstractControl {
    return this.registerForm.get('name')!;
  }
  get email(): AbstractControl {
    return this.registerForm.get('email')!;
  }
  get phone(): AbstractControl {
    return this.registerForm.get('phone')!;
  }
  get findus(): AbstractControl {
    return this.registerForm.get('findus')!;
  }
}
