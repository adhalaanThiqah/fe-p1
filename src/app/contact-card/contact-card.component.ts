import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-contact-card',
  templateUrl: './contact-card.component.html',
  styleUrls: ['../../assets/scss/contact-card.component.scss'],
})
export class ContactCardComponent {
  @Input() icon_url: string;
  @Input() type: string;
  @Input() contact: string;

  constructor() {
    this.icon_url = '';
    this.type = '';
    this.contact = '';
  }
}
